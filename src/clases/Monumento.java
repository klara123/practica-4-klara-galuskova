package clases;

import java.time.LocalDate;

public class Monumento {

	// Atributos
	private String nombreMonumento;
	private String codigo;
	private String estilo;
	private String material;
	private int siglo;
	private double altura;
	private double ancho;
	private double largo;
	private double precio;
	private LocalDate fecha;

	// Constructores
	protected Monumento() {
		this.nombreMonumento = "nombreMonumento";
		this.estilo = "estilo";
		this.material = "material";
		this.siglo = 0;
		this.altura = 0;
		this.ancho = 0;
		this.largo = 0;
		this.precio = 0;
		this.fecha = LocalDate.now();
	}

	protected Monumento(String nombreMonumento, String codigo) {
		this.nombreMonumento = nombreMonumento;
		this.codigo = codigo;
		this.fecha = LocalDate.now();
	}

	protected Monumento(String nombreMonumento, String codigo, String estilo, String material, int siglo, double altura,
			double ancho, double largo, double precio) {
		this(nombreMonumento, codigo);
		this.estilo = estilo;
		this.material = material;
		this.siglo = siglo;
		this.altura = altura;
		this.ancho = ancho;
		this.largo = largo;
		this.precio = precio;
	}

	// Getters y setters
	public String getNombreMonumento() {
		return nombreMonumento;
	}

	public void setNombreMonumento(String nombreMonumento) {
		this.nombreMonumento = nombreMonumento;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public int getSiglo() {
		return siglo;
	}

	public void setSiglo(int siglo) {
		this.siglo = siglo;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getAncho() {
		return ancho;
	}

	public void setAncho(double ancho) {
		this.ancho = ancho;
	}

	public double getLargo() {
		return largo;
	}

	public void setLargo(double largo) {
		this.largo = largo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	// m�todo toString
	@Override
	public String toString() {
		return "Monumento [nombreMonumento=" + nombreMonumento + ", codigo=" + codigo + ", estilo=" + estilo
				+ ", material=" + material + ", siglo=" + siglo + ", altura=" + altura + ", ancho=" + ancho + ", largo="
				+ largo + ", precio=" + precio + ", fecha=" + fecha + "]";
	}

}
