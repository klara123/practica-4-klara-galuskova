package clases;

public class CascoHistorico {

	// Atributos
	private Monumento[] monumentos; // array para almacenar los monumentos creados
	private String ciudad;
	private String nombreCascoHistorico;
	private int cantidadMonumentos;

	// Constructores
	public CascoHistorico() {
		this.monumentos = new Monumento[4];
		this.ciudad = "Zaragoza";
		this.nombreCascoHistorico = "Casco Viejo";
		this.cantidadMonumentos = 4;
	}

	protected CascoHistorico(int cantidadMonumentos, String ciudad) {
		this.monumentos = new Monumento[cantidadMonumentos];
		this.ciudad = ciudad;
	}

	public CascoHistorico(int cantidadMonumentos, String ciudad, String nombreCascoHistorico) {
		this(cantidadMonumentos, ciudad);
		this.nombreCascoHistorico = nombreCascoHistorico;
		this.cantidadMonumentos = cantidadMonumentos;
	}

	// Getters y setters
	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getNombreCascoHistorico() {
		return nombreCascoHistorico;
	}

	public void setNombreCascoHistorico(String nombreCascoHistorico) {
		this.nombreCascoHistorico = nombreCascoHistorico;
	}

	public int getCantidadMonumentos() {
		return cantidadMonumentos;
	}

	public void setCantidadMonumentos(int cantidadMonumentos) {
		this.cantidadMonumentos = cantidadMonumentos;
	}

	// M�todos de la clase CascoHistorico
	public void altaMonumentos(String nombre, String codigo, String estilo, String material, int siglo, double altura,
			double ancho, double largo, double precio) {

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] == null) {
				monumentos[i] = new Monumento(nombre, codigo, estilo, material, siglo, altura, ancho, largo, precio);
				break;
			}
		}

	}

	public void altaMonumentos(String nombre, String codigo) {

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] == null) {
				monumentos[i] = new Monumento(nombre, codigo);
				break;
			}
		}

	}

	public boolean buscarHuecoMonumentos() {

		/* El m�todo busca un hueco en el array monumentos */
		/* Si devuelve 'true' se permite al usuario dar de alta a un nuevo monumento */
		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] == null) {
				return true;
			}
		}

		return false;

	}

	public void listarMonumentos() {

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] != null) {
				System.out.println("(" + (i + 1) + "/" + monumentos.length + ") " + monumentos[i]);
			} else {
				System.out.println("(" + (i + 1) + "/" + monumentos.length + ") ");
			}
		}

	}

	public boolean listarMonumentosPorEstilo(String estilo) {

		estilo = estilo.toLowerCase();
		boolean monumentoEncontrado = false;

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] != null) {
				if (monumentos[i].getEstilo().toLowerCase().contains(estilo)) {
					monumentoEncontrado = true;
					System.out.println(monumentos[i]);
				}
			}
		}

		return monumentoEncontrado;

	}

	public boolean eliminarMonumentoPorCodigo(String codigo) {

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] != null) {
				if (monumentos[i].getCodigo().equals(codigo)) {
					monumentos[i] = null;
					return true;
				}
			}
		}

		return false;

	}

	public void cambiarPrecio(String cod, double precio) {

		boolean monumentoEncontrado = false;

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] != null) {
				if (monumentos[i].getCodigo().equals(cod)) {
					monumentoEncontrado = true;
					double aux = monumentos[i].getPrecio();
					monumentos[i].setPrecio(precio);
					System.out.println("Precio del monumento '" + cod + "' cambiado de " + aux + " a "
							+ monumentos[i].getPrecio());
				}
			}
		}

		if (!monumentoEncontrado) {
			System.out.println("El monumento '" + cod + "' no encontrado");
		}

	}

	public Monumento buscarMonumentoPorNombre(String nombre) {

		nombre = nombre.toLowerCase();

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] != null) {
				if (monumentos[i].getNombreMonumento().toLowerCase().equals(nombre)) {
					return monumentos[i];
				}
			}
		}

		return null;

	}

	public void calcularPrecioConIva(int iva) {

		double precio;

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] != null) {
				precio = monumentos[i].getPrecio();
				if (precio == 0) {
					System.out.println(
							"(" + (i + 1) + "/" + monumentos.length + ") La entrada a este monumento es gratis");
				} else {
					System.out.println("(" + (i + 1) + "/" + monumentos.length + ") El precio es "
							+ (precio += (precio / 100 * iva)));
				}
			} else {
				System.out.println("(" + (i + 1) + "/" + monumentos.length + ") El monumento no est� creado ");
			}
		}

	}

	public void mostrarSuperficie() {

		for (int i = 0; i < monumentos.length; i++) {
			if (monumentos[i] != null) {
				System.out.println("(" + (i + 1) + "/" + monumentos.length + ") Superficie: "
						+ monumentos[i].getAncho() * monumentos[i].getLargo() + " m\u00B2");
			} else {
				System.out.println("(" + (i + 1) + "/" + monumentos.length + ") El monumento no est� creado");
			}
		}

	}

}
