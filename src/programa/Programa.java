package programa;

import java.util.Scanner;

import clases.CascoHistorico;
import clases.Monumento;

public class Programa {

	static Scanner input = new Scanner(System.in);
	static Programa programa = new Programa();

	public static void main(String[] args) {

		System.out.println("______________________________________________");
		System.out.println("            Registro de monumentos");
		System.out.println("______________________________________________");

		CascoHistorico cascoHistorico = null;
		char opcionCreacion;

		do {

			/*
			 * Se puede elegir entre usar monumentos del programa ya creados, y crear nuevos
			 * monumentos
			 */
			/*
			 * Hay que intoducir el nombre del casco hist�rico, el nombre de la ciudad, y la
			 * cantidad de monumentos que vamos a almacenar
			 */
			/*
			 * Al introducir esos datos, podemos elegir entre introducir datos b�sicos (solo
			 * el nombre y el c�digo) y introducir datos extendidos (nombre, c�digo, estilo,
			 * material, siglo, altura, ancho, largo y precio
			 */
			opcionCreacion = programa.listarOpcionesMonumentos();

			if (opcionCreacion == '1') {
				cascoHistorico = programa.crearCasco();
			} else if (opcionCreacion == '2') {
				cascoHistorico = programa.crearCascoConDatos();
			} else {
				System.out.println("La opci�n '" + opcionCreacion + "' no es v�lida");
			}

		} while (opcionCreacion != '1' && opcionCreacion != '2');

		char opcion;

		/* Despu�s de crear los monumentos, se pinta el men� hasta introducir el '0' */
		do {

			opcion = programa.listarMenu();

			switch (opcion) {

			case '1':
				/* A�adir un monumento con datos b�sicos */
				if (cascoHistorico.buscarHuecoMonumentos()) {
					programa.introducirDatosBasicos(cascoHistorico, 1);
				} else {
					System.out.println(
							"No hay espacio para almacenar otro monumento. Primero tienes que eliminar un monumento.");
				}

				break;

			case '2':
				/* A�adir un monumento con datos extendidos */
				if (cascoHistorico.buscarHuecoMonumentos()) {
					programa.introducirDatosExtendidos(cascoHistorico, 1);
				} else {
					System.out.println(
							"No hay espacio para almacenar otro monumento. Primero tienes que eliminar un monumento.");
				}
				break;

			case '3':
				/* Listar todos los monumentos */
				cascoHistorico.listarMonumentos();
				break;

			case '4':
				/* Eliminar monumento por c�digo */
				System.out.println("\nIntroduce el c�digo del monumento para eliminar: ");
				String cod1 = input.nextLine();
				boolean eliminado = cascoHistorico.eliminarMonumentoPorCodigo(cod1);

				if (eliminado) {
					System.out.println("Monumento '" + cod1 + "' eliminado");
				} else {
					System.out.println("Monumento '" + cod1 + "' no encontrado");
				}

				break;

			case '5':
				/* Listar monumentos por estilo: */
				System.out.println(
						"\nIntroduce el estilo del monumento o parte de la palabra (no importan may�sculas): ");
				String estilo = input.nextLine();

				boolean encontrado = cascoHistorico.listarMonumentosPorEstilo(estilo);

				if (!encontrado) {
					System.out.println("Monumento con estilo '" + estilo + "' no encontrado.");
				}

				break;

			case '6':
				/* Cambiar precio: */
				System.out.println("\nIntroduce el c�digo del monumento cuyo precio quieres cambiar:");
				String cod6 = input.nextLine();
				System.out.println("Introduce el nuevo precio:");
				double precio = input.nextDouble();

				cascoHistorico.cambiarPrecio(cod6, precio);

				input.nextLine(); // limpio el buffer
				break;

			case '7':
				/* Buscar monumento por nombre: */
				System.out.println("\nIntroduce el nombre del monumento que quieres encontrar:");
				String nombreMonumento = input.nextLine();

				Monumento monumento = cascoHistorico.buscarMonumentoPorNombre(nombreMonumento);

				if (monumento != null) {
					System.out.println("Informaci�n del monumento '" + nombreMonumento + "':");
					System.out.println(monumento);
				} else {
					System.out.println("El monumento con el nombre '" + nombreMonumento + "' no encontrado");
				}

				break;

			case '8':
				/* Mostrar superficie de los monumentos */
				cascoHistorico.mostrarSuperficie();
				break;

			case '9':
				/* Calcular precio con IVA */
				/* El m�todo solo muestra precios con IVA de cada monumento, no los cambia */
				System.out.println("Introduce el IVA que quieres aplicar:");
				System.out.println("     1 - IVA general (14%)");
				System.out.println("     2 - IVA reducido (10%)");
				char opcionIva = input.nextLine().charAt(0);

				if (opcionIva != '1' && opcionIva != '2') {
					System.out.println("La opci�n '" + opcionIva + "' no es v�lida");
				} else {
					if (opcionIva == '1') {
						cascoHistorico.calcularPrecioConIva(14);
					} else {
						cascoHistorico.calcularPrecioConIva(10);
					}
				}

				break;

			case '0':
				System.out.println("______________________________________________");
				System.out.println("            Programa terminado");
				System.out.println("______________________________________________");
				input.close(); // cierro el esc�ner
				System.exit(0);
				break;

			default:
				System.out.println("______________________________________________");
				System.out.println("          La opci�n '" + opcion + "' no es v�lida");
				System.out.println("______________________________________________");

			}

		} while (opcion != '0');

	}

	/* Opciones del men�: */
	private char listarMenu() {
		System.out.println("\nElige una opci�n del men�:");
		System.out.println("     0 - Terminar programa");
		System.out.println("     1 - A�adir monumento con informaci�n b�sica");
		System.out.println("     2 - A�adir monumento con informaci�n extendida");
		System.out.println("     3 - Listar monumentos");
		System.out.println("     4 - Eliminar por c�digo");
		System.out.println("     5 - Listar por estilo");
		System.out.println("     6 - Cambiar precio");
		System.out.println("     7 - Buscar monumento por nombre");
		System.out.println("     8 - Calcular superficie de los monumentos");
		System.out.println("     9 - Calcular precio con IVA");
		return input.nextLine().charAt(0);
	}

	private char listarOpcionesMonumentos() {
		System.out.println("\nElige una opci�n: ");
		System.out.println("     1 - Usar monumentos del programa");
		System.out.println("     2 - Crear nuevos monumentos");
		return input.nextLine().charAt(0);
	}

	/*
	 * M�todo para crear el casco con monumentos del Casco Viejo en Zaragoza (se
	 * llama al elegir la opci�n de usar los monumentos del programa)
	 */
	private CascoHistorico crearCasco() {

		CascoHistorico cascoViejo = new CascoHistorico();

		cascoViejo.altaMonumentos("Catedral del Salvador", "MZ1", "rom�nico, g�tico, mud�jar", "piedra, ladrillo", 12,
				20, 10, 60, 6.66);
		cascoViejo.altaMonumentos("Muralla romana", "MZ2", "rom�nico", "piedra", 1, 10, 4, 80, 0);
		cascoViejo.altaMonumentos("Iglesia de San Pablo", "MZ3", "mud�jar", "ladrillo", 14, 15, 10, 30, 2.3);
		cascoViejo.altaMonumentos("Iglesia de Santa Mar�a Magdalena", "MZ4", "mud�jar", "ladrillo", 14, 13, 10, 55,
				5.1);

		return cascoViejo;
	}

	private CascoHistorico crearCascoConDatos() {

		System.out.println("\nIntroduce el nombre de la ciudad:");
		String ciudad = input.nextLine();
		System.out.println("Introduce el nombre de su casco hist�rico:");
		String casco = input.nextLine();
		System.out.println("Introduce la cantidad de monumentos que quieres almacenar:");
		int tamano = input.nextInt();

		CascoHistorico cascoHistorico = new CascoHistorico(tamano, ciudad, casco);

		input.nextLine(); // limpio el buffer

		char opcDatos = programa.elegirCantidadDatos();

		if (opcDatos == '1') {
			programa.introducirDatosBasicos(cascoHistorico, cascoHistorico.getCantidadMonumentos());
		} else if (opcDatos == '2') {
			programa.introducirDatosExtendidos(cascoHistorico, cascoHistorico.getCantidadMonumentos());
		}

		return cascoHistorico;

	}

	public char elegirCantidadDatos() {

		System.out.println("\nElige la cantidad de datos que quieres almacenar de cada monumento:");
		System.out.println("     1 - Introducir datos b�sicos");
		System.out.println("     2 - Introducir datos extendidos");

		return input.nextLine().charAt(0);

	}

	public void introducirDatosBasicos(CascoHistorico cascoHistorico, int cant) {

		for (int i = 0; i < cant; i++) {

			System.out.println("Introduce el nombre del monumento: (" + (i + 1) + "/" + cant + ")");
			String nombreMonumento = input.nextLine();
			System.out.println("Introduce el c�digo: (" + (i + 1) + "/" + cant + ")");
			String codigo = input.nextLine();

			cascoHistorico.altaMonumentos(nombreMonumento, codigo);

		}

	}

	public void introducirDatosExtendidos(CascoHistorico cascoHistorico, int cant) {

		for (int i = 0; i < cant; i++) {

			System.out.println("Introduce el nombre del monumento: (" + (i + 1) + "/" + cant + ")");
			String nombreMonumento = input.nextLine();
			System.out.println("Introduce el c�digo: (" + (i + 1) + "/" + cant + ")");
			String codigo = input.nextLine();
			System.out.println("Introduce el estilo: (" + (i + 1) + "/" + cant + ")");
			String estilo = input.nextLine();
			System.out.println("Introduce el material: (" + (i + 1) + "/" + cant + ")");
			String material = input.nextLine();
			System.out.println("Introduce el siglo (con cifras): (" + (i + 1) + "/" + cant + ")");
			int siglo = input.nextInt();
			System.out.println("Introduce la altura: (" + (i + 1) + "/" + cant + ")");
			double altura = input.nextDouble();
			System.out.println("Introduce el ancho: (" + (i + 1) + "/" + cant + ")");
			double ancho = input.nextDouble();
			System.out.println("Introduce el largo: (" + (i + 1) + "/" + cant + ")");
			double largo = input.nextDouble();
			System.out.println("Introduce el precio de la entrada: (" + (i + 1) + "/" + cant + ")");
			double precio = input.nextDouble();

			cascoHistorico.altaMonumentos(nombreMonumento, codigo, estilo, material, siglo, altura, ancho, largo,
					precio);

			input.nextLine(); // limpio el buffer

		}

	}

}
